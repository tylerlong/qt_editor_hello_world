#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <QtGui>

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow();
    
private slots:
     void open();
     void save();
     void quit();

 private:
     QMenu *fileMenu;
     QAction *openAction;
     QAction *saveAction;
     QAction *exitAction;
     QTextEdit *textEdit;

};

#endif // MAIN_WINDOW_H
