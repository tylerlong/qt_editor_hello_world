===========
Tisp Editor
===========

Tisp is short for Tidy Lisp.

Tisp Editor enables you to write clean Lisp code.

Tisp is written in Qt, so it is available on Windows, Mac and Linux.
